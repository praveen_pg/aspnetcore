using DevOpsDemo.Controllers;
using DevOpsDemo.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace DevOpsDemo.Test
{
    public class PostTestController
    {
        private PostRepository repository;

        public PostTestController()
        {
            repository = new PostRepository();
        }

        [Fact]
        public void Test_Index_View_Result()
        {
            var controller = new HomeController(this.repository);

            var result = controller.Index();

            Assert.IsType<ViewResult>(result);
        }
    }
}
